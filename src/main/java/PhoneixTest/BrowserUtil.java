package PhoneixTest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import java.net.MalformedURLException;
import java.net.URL;

public class BrowserUtil {
    public static final String USERNAME = (System.getenv("BROWSERSTACK_USERNAME") != null) ? System.getenv("BROWSERSTACK_USERNAME") : "aravindhseethara_EaKQ72";
    public static final String AUTOMATE_KEY = (System.getenv("BROWSERSTACK_ACCESS_KEY") != null) ? System.getenv("BROWSERSTACK_ACCESS_KEY") : "qM2Wsb7SMinpqNMc5VAu";
    // declare remote URL in a variable
    public static final String HUB_URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub.browserstack.com/wd/hub";

    private static WebDriver wd;

    public BrowserUtil(Envi env, Browser b) throws MalformedURLException {
        if(env == Envi.LOCAL){
            if (b==Browser.CHROME){
                WebDriverManager.chromedriver().setup();
                wd = new ChromeDriver();
                System.out.println("we");
            }
            else if(b == Browser.FIREFOX){
                WebDriverManager.firefoxdriver().setup();
                wd=new FirefoxDriver();
            }
        }

        if(env == Envi.REMOTE){
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser","chrome");
            caps.setCapability("browser_version","106.0");
            caps.setCapability("os", "OS X");
            caps.setCapability("os_version", "Big Sur");
            caps.setCapability("name","Sample Test222");
            caps.setCapability("build"," BS Number22");
            wd =new RemoteWebDriver(new URL(HUB_URL),caps);
        }
        setWd(wd);

    }

    public BrowserUtil() {

    }

    public static WebDriver getWd() {
        return wd;
    }
    public void setWd(WebDriver wd) {
        BrowserUtil.wd = wd;
    }


    public BrowserUtil(WebDriver wd) {
        super();
        this.wd = wd;
    }


    public void enterKeys(By Wbelement ,String creds ){
        WebElement webElementUserNameLocator = wd.findElement(Wbelement);
        webElementUserNameLocator.clear();
        webElementUserNameLocator.sendKeys(creds);
    }

    public void clickable(By Wbelement){
        WebElement webElementUserNameLocator = wd.findElement(Wbelement);
        webElementUserNameLocator.click();
    }

    public String readText(By Wbelement){
        WebElement webElementUserNameLocator = wd.findElement(Wbelement);
       String myVal =  webElementUserNameLocator.getText();
       return myVal;
    }

    public void SelectElements(By WebElementSelectLocator,String item){
        Select webElementSelectors = new Select(wd.findElement(WebElementSelectLocator));
        webElementSelectors.selectByValue(item);
    }

    public void getURL(String myURl){
        wd.get(myURl);
    }

}
