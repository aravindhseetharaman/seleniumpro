package PhoneixTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;

import java.net.MalformedURLException;

public class CreateJob extends BrowserUtil{
    private static final By OEMElement_ = By.xpath("//span[contains(text(),'Select OEM')]/../../../../..");
    private static final By OEM_LISTBOX = By.xpath("//span[@class='mat-option-text']");
    private static final By FIRSTNAMETEXTBOXLOCATOR = By.xpath("//input[@id='mat-input-6']");

    public CreateJob() {
        super();
    }

    public CreateJob(Browser b) {
        super();
    }
    public CreateJob(Envi env,Browser b) throws MalformedURLException {
        super(env, b);
    }
    public CreateJob(WebDriver wb) {
        super(wb);
    }
    public void selectorx() throws InterruptedException {
        Thread.sleep(3000);
        clickable(OEMElement_);
        Thread.sleep(5000);
      //  SelectElements(OEM_LISTBOX,"Redmi");
      //  System.out.println("SElected...");
    }

    public void enterFirstName(){
        enterKeys(FIRSTNAMETEXTBOXLOCATOR,"AKSHATH");
        System.out.println("entered akshath");

    }


}
