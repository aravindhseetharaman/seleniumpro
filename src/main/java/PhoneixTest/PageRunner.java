package PhoneixTest;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;

import java.net.MalformedURLException;

public class PageRunner extends BrowserUtil{

    public PageRunner(Envi env, Browser b) throws MalformedURLException {
        super(env, b);
    }

    public PageRunner(WebDriver wd) {
        super(wd);
    }


    public LoginPage loadPage(Envi e,Browser b) throws InterruptedException, MalformedURLException {
        getURL("http://phoenix.testautomationacademy.in/");
        LoginPage loginPage = new LoginPage();
        return loginPage;
    }


}
