package PhoneixTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;

import java.net.MalformedURLException;

public class HomePagePhx extends BrowserUtil{
    private static final By CREATEJOB_ELEMENT = By.xpath( "//span[contains(text(),' Create Job ')]/../../../..");
    private static final By SIGNINASLOCATOR = By.xpath("(//*[name()='svg'])[8]");
    private static final By SIGNED_IN_ROLE = By.xpath("//span[normalize-space()=\"iamfd\"]");
    public HomePagePhx() {
    }


    public HomePagePhx(Envi e, Browser b) throws MalformedURLException {
        super(e,b);

    }

    public HomePagePhx(WebDriver wb) {
        super(wb);
    }

    public String signInasLocator() throws InterruptedException {
        Thread.sleep(5000);
        clickable(SIGNINASLOCATOR);
        String loggedinRole = readText(SIGNED_IN_ROLE);
        return loggedinRole;
    }

    public CreateJob clickcreateJob() throws InterruptedException {
        Thread.sleep(3000);
        clickable(CREATEJOB_ELEMENT);
        CreateJob createJob = new CreateJob();
        return createJob;

    }


}
